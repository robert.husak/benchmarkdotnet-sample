﻿using BenchmarkDotNet.Running;

namespace BenchmarkDotNetSample;

public partial class Program
{
    public static void Main(string[] args)
    {
        BenchmarkRunner.Run(typeof(Program).Assembly, args: args);

        // Hard-coded variants:
        //BenchmarkRunner.Run<ArrayIterationBenchmark>();
        //BenchmarkRunner.Run<StringParsingBenchmark>();
    }
}