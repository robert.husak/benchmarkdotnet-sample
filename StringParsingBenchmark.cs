﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Jobs;

namespace BenchmarkDotNetSample;

[Config(typeof(Config))]
[MemoryDiagnoser]
[PlainExporter]
[RPlotExporter]
public class StringParsingBenchmark
{
    private class Config : ManualConfig
    {
        public Config()
        {
            AddJob(Job.Default);
            AddJob(Job.Default
                .WithGcForce(false));
        }
    }

    private const string _line = "Lorem Ipsum;125;2556;7563;15467;2546;42";

    [Benchmark(Baseline = true)]
    public int ParseValueByStringSplit()
    {
        var tokens = _line.Split(';');
        var lastToken = tokens[tokens.Length - 1];

        return int.Parse(lastToken);
    }

    [Benchmark]
    public int ParseValueBySubstring()
    {
        var lastSemicolonPos = _line.LastIndexOf(';');
        var lastToken = _line.Substring(lastSemicolonPos + 1);

        return int.Parse(lastToken);
    }

#if NETCOREAPP

    [Benchmark]
    public int ParseValueBySpan()
    {
        var lastSemicolonPos = _line.LastIndexOf(';');
        var lastTokenSpan = _line.AsSpan(lastSemicolonPos + 1);

        return int.Parse(lastTokenSpan);
    }

#endif
}
