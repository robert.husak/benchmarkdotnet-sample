﻿using BenchmarkDotNet.Attributes;

namespace BenchmarkDotNetSample;

[SimpleJob(BenchmarkDotNet.Jobs.RuntimeMoniker.Net60)]
[SimpleJob(BenchmarkDotNet.Jobs.RuntimeMoniker.Net461)]
public class ArrayIterationBenchmark
{
    private int[] _array = new int[100];

    public ArrayIterationBenchmark()
    {
        for (var i = 0; i < _array.Length; i++)
        {
            _array[i] = i;
        }
    }

    [Benchmark(Baseline = true)]
    public int IterateArrayWithFor()
    {
        var sum = 0;
        for (var i = 0; i < _array.Length; i++)
        {
            sum += _array[i];
        }

        return sum;
    }

    [Benchmark]
    public int IterateArrayWithForeach()
    {
        var sum = 0;
        foreach (var val in _array)
        {
            sum += val;
        }

        return sum;
    }

    [Benchmark]
    public int IterateIEnumerableWithForeach()
    {
        IEnumerable<int> enumerable = _array;

        var sum = 0;
        foreach (var val in enumerable)
        {
            sum += val;
        }

        return sum;
    }
}